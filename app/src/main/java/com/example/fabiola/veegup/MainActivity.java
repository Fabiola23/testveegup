package com.example.fabiola.veegup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;

import org.json.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView lv;
    ArrayList<String> image;
    ArrayList<String> text;
    ArrayList<String> meal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        image = new ArrayList<String>();
        text = new ArrayList<String>();
        meal = new ArrayList<String>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = (ListView) findViewById(R.id.list_products);

        //lecture fichier
        String fichier = "test.json";

        String chaine = "";

        //lecture du fichier texte
        try {
            InputStream ips = getAssets().open(fichier);
            InputStreamReader ipsr = new InputStreamReader(ips);
            BufferedReader br = new BufferedReader(ipsr);
            String ligne;
            while ((ligne = br.readLine()) != null) {
                //System.out.println(ligne);
                chaine += ligne + "\n";
            }
            br.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
            int cmpt = 1; //compteur des keys dans tableau
            JSONArray tableau = new JSONArray(chaine);
            System.out.println("taille tableau "+tableau);
            for (int i = 0; i < tableau.length(); i++) { //parser les tables 1, 2, 3 etc...
                JSONObject c = tableau.getJSONObject(i);

                while(c.has(cmpt+"")){
                    String ph = c.getString("" + cmpt); //parser les contenus des tables
                    JSONObject p = new JSONObject(ph);

                    String photo = p.getString("img");
                    String nom = p.getString("title");
                    String m = p.getString("meal_cat");
                    image.add(photo);
                    text.add(nom);
                    meal.add(m);
                    System.out.println("meal "+m);
                    cmpt++;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MonAdapter adapter = new MonAdapter(text, image, meal, this.getApplicationContext());
        lv.setAdapter(adapter);
    }
}




