package com.example.fabiola.veegup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by fabiola on 17/05/18.
 */

class MonAdapter extends BaseAdapter {

    String[] data_text;
    String[] data_image;
    String[] data_meal;
    Context context;

    MonAdapter(ArrayList<String> text, ArrayList<String> image, ArrayList<String> meal, Context context) {
        this.context = context;
        data_text = new String[text.size()];
        data_image = new String[image.size()];
        data_meal = new String[meal.size()];

        for (int i = 0; i < text.size(); i++) {
            data_text[i] = text.get(i);
            data_image[i] = image.get(i);
            data_meal[1] = meal.get(i);
        }

    }

    public int getCount() {
        return data_text.length;
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row;

        row = inflater.inflate(R.layout.recette, parent, false);

        TextView textview = (TextView) row.findViewById(R.id.title);
        ImageView imageview = (ImageView) row.findViewById(R.id.image);

        textview.setText(data_text[position]);
        //imageview.setImageResource(data_image[position]);
        ImageDownloader imageDownloader = (ImageDownloader) new ImageDownloader(imageview).execute(data_image[position]);


        return (row);

    }

}

